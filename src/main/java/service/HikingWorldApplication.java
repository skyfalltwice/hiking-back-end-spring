package service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HikingWorldApplication {

	public static void main(String[] args) {
		SpringApplication.run(HikingWorldApplication.class, args);
	}

}
