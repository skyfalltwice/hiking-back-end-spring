package service;


import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("generic")
public class Webservices 
{
	Services services;
	
	public Webservices()
	{
		services = new Services();
	}
	
	@GET
	@Path("world")
	@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
	public Response getWorld(@Context HttpServletRequest request)
	{
		String username = request.getHeader("X-User");
		return Response.ok(services.getWorld()).build();
	}
}
