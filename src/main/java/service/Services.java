package service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import generated.PallierType;
import generated.ProductType;
import generated.World;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Gestion des mondes XML
 */
public class Services {

	private InputStream getPlainWorld() {
		return getClass().getClassLoader().getResourceAsStream("world.xml");
	}

	public World readWorldFromXml(String username) {
		// TODO fix: username always null
		World world = null;
		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance(World.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			InputStream inputStreamFichierMonde;
			inputStreamFichierMonde = getPlainWorld();
			world = (World) unmarshaller.unmarshal(inputStreamFichierMonde);
			inputStreamFichierMonde.close();

		} catch (JAXBException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}

		return world;

	}

	public void saveWorldToXml(World world, String name) {
		// TODO fix: username always null
		if (name != null) {
			try {
				JAXBContext jc = JAXBContext.newInstance(World.class);
				Marshaller marshaller = jc.createMarshaller();

				OutputStream output = new FileOutputStream(name + "-world.xml");
				marshaller.marshal(world, output);

				output.close();

			} catch (JAXBException e) {
				// Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public World getWorld() {
		return readWorldFromXml(null);
	}
	

	public World getWorld(String username) throws IOException, JAXBException, IllegalArgumentException {
		World world = readWorldFromXml(username);
		// FIX
		saveWorldToXml(world, username);
		return world;
	}

}
