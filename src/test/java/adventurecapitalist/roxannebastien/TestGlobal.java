package adventurecapitalist.roxannebastien;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import generated.World;
import service.Services;

/**
 * Classe regroupant un ensemble de test pour controler le fonctionnement du code.
 * FIXME fait à l'arrache.
 * 
 */
class TestGlobal {

	/**
	 * On peut lire le monde d'origine sans lever une exception
	 */
	@Test
	void testLectureMondeVierge() 
	{
		Services services = new Services();
		World world = services.getWorld();	
		
		assertNotEquals(null, world);
	}

}
